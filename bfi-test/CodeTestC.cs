﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bfi_test
{
    public class CodeTestC
    {
        private const string X = "X";
        private const string Y = "Y";
        private const string Z = "Z";
        private const string Separator = " ";

        public string Print(int input)
        {
            var sb = new StringBuilder();
            for (int i = 1; i <= input; i++)
            {
                string temp = "";

                if (i % 3 == 0)
                    temp += X;
                
                if (i % 5 == 0)
                    temp += Y;

                if (i % 7 == 0)
                    temp += Z;

                if (temp == "")
                    temp = i.ToString();

                sb.Append(temp);
                sb.Append(Separator);
            }

            return sb.ToString().Trim();
        }
    }
}