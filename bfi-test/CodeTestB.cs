﻿using System;

namespace bfi_test
{
    public class CodeTestB
    {
        private const int RandomLength = 12;
        private const string RandomChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        public string Rand()
        {
            string result = "";

            // generate 12 char random alphanumeric
            var random = new Random();
            for (int i = 0; i < RandomLength; i++)
            {
                result += RandomChar[random.Next(0, RandomChar.Length)];
            }

            // insert 2 digit date on position 8 and 2 digit month on the end
            result = result.Insert(7, DateTime.Today.ToString("dd")) + DateTime.Today.ToString("MM");

            return result;
        }
    }
}
