﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace bfi_test
{
    public class City
    {
        public string Country { get; set; }

        public string Name { get; set; }
    }
    public class CodeTestA
    {
        private const string CitiesUrl = "https://raw.githubusercontent.com/lutangar/cities.json/master/cities.json";
        private const string CountryCode = "ID";

        private IEnumerable<City> _cities;

        public async Task<IEnumerable<string>> Search(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                throw new ArgumentNullException(nameof(input));

            var result = new List<string>();

            if (_cities == null)
            {
                // download cities database from remote url
                var cities = await DownloadData();

                if (cities == null)
                    return result;

                // filter country
                _cities = FilterContry(cities);
            }

            result = SearchSameLength(input);
            result.AddRange(SearchContains(input));

            return result.Distinct();
        }

        private async Task<IEnumerable<City>> DownloadData()
        {
            string strJson;

            using (var webClient = new WebClient())
            {
                strJson = await webClient.DownloadStringTaskAsync(CitiesUrl);
            }

            if (string.IsNullOrWhiteSpace(strJson))
                return null;

            return JsonConvert.DeserializeObject<List<City>>(strJson);
        }

        private IEnumerable<City> FilterContry(IEnumerable<City> cities)
        {
            return cities.Where(x => x.Country.Equals(CountryCode));
        }

        private List<string> SearchSameLength(string input)
        {
            var sameLenthCities = _cities.Where(x => x.Name.Length == input.Length).ToList();

            var result = new List<string>();

            Parallel.ForEach(sameLenthCities, (city) =>
            {
                if (IsAcceptableError(input, city.Name))
                {
                    result.Add(city.Name);
                }
            });

            return result;
        }

        private bool IsAcceptableError(string input1, string input2)
        {
            int maxError = input1.Length / 4;
            int errorCount = 0;
            input1 = input1.ToLower();
            input2 = input2.ToLower();
            for (int i = 0; i < input1.Length; i++)
            {
                if (input1[i] != input2[i])
                {
                    errorCount += 1;
                    if (errorCount > maxError)
                        break;
                }
            }
            
            return errorCount <= maxError;
        }

        private List<string> SearchContains(string input)
        {
            var sameLenthCities = _cities.Where(x => x.Name.Length != input.Length).ToList();

            var result = new List<string>();

            Parallel.ForEach(sameLenthCities, (city) =>
            {
                if (Contains(input, city.Name))
                {
                    result.Add(city.Name);
                }
            });

            return result;

        }

        private bool Contains(string input, string city)
        {
            input = input.ToLower();
            city = city.ToLower();
            
            return input.Contains(city);
        }
    }
}
