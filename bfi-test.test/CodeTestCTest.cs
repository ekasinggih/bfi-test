﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace bfi_test.test
{
    [TestClass]
    public class CodeTestCTest
    {
        [TestMethod]
        public void Test15()
        {
            var expected = "1 2 X 4 Y X Z 8 X Y 11 X 13 Z XY";
            var input = 15;
            var testCode = new CodeTestC();
            var actual = testCode.Print(input);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test200()
        {
            var input = 200;
            var testCode = new CodeTestC();
            var result = testCode.Print(input);
            var array = result.Split(" ");

            for (int i = 0; i < array.Length; i++)
            {
                int number;
                if (int.TryParse(array[i], out number))
                {
                    if (number != i + 1)
                        Assert.IsTrue(false);
                }
                else
                {
                    if (array[i].Contains("X") && (i + 1) % 3 != 0)
                    {
                        Assert.IsTrue(false);
                    }
                    if (array[i].Contains("Y") && (i + 1) % 5 != 0)
                    {
                        Assert.IsTrue(false);
                    }
                    if (array[i].Contains("Z") && (i + 1) % 7 != 0)
                    {
                        Assert.IsTrue(false);
                    }
                }
            }

            if (!result.Contains("1 2 X 4 Y "))
                Assert.IsTrue(false);

            if (!result.Contains(" 19 Y XZ 22 "))
                Assert.IsTrue(false);

            if (!result.Contains(" 104 XYZ 106 "))
                Assert.IsTrue(false);

            if (!result.Contains(" 199 Y"))
                Assert.IsTrue(false);
        }
    }
}
