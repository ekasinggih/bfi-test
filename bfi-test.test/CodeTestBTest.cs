﻿using System;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace bfi_test.test
{
    [TestClass]
    public class CodeTestBTest
    {
        [TestMethod]
        public void SixTeenCharLength()
        {
            var codeTest = new CodeTestB();
            var random = codeTest.Rand();
            Assert.AreEqual(random.Length, 16);
        }

        [TestMethod]
        public void AlphaNumericOnly()
        {
            var codeTest = new CodeTestB();
            var random = codeTest.Rand();

            Regex r = new Regex("^[a-zA-Z0-9]*$");
            Assert.IsTrue(r.IsMatch(random));
        }

        [TestMethod]
        public void CurrentDateOnEightAndNineChar()
        {
            var codeTest = new CodeTestB();
            var random = codeTest.Rand();

            var expedted = DateTime.Today.ToString("dd");
            var actual = random.Substring(7, 2);
            Assert.AreEqual(expedted, actual);
        }

        [TestMethod]
        public void CurrentMonthOnLastTwoChar()
        {
            var codeTest = new CodeTestB();
            var random = codeTest.Rand();

            var expedted = DateTime.Today.ToString("MM");
            var actual = random.Substring(14);
            Assert.AreEqual(expedted, actual);
        }
    }
}
