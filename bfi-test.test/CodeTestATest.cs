using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace bfi_test.test
{
    [TestClass]
    public class CodeTestATest
    {
        CodeTestA codeTest = new CodeTestA();

        [TestMethod]
        public async Task TestInput1()
        {
            var input = "sisayang";
            var expedted = new[] { "Sinabang", "Cisayong", "Birayang", "Senayang" };
            var actual = await codeTest.Search(input);
            CollectionAssert.AreEqual(expedted, actual.ToArray());
        }

        [TestMethod]
        public async Task TestInput2()
        {
            var input = "pulxu pqnjzng";
            var expedted = new[] { "Pulau Punjung" };
            var actual = await codeTest.Search(input);
            CollectionAssert.AreEqual(expedted, actual.ToArray());
        }

        [TestMethod]
        public async Task TestInput3()
        {
            var input = "zogjakarta";
            var expedted = new[] { "Yogyakarta", "Jakarta" };
            var actual = await codeTest.Search(input);
            CollectionAssert.AreEqual(expedted, actual.ToArray());
        }
    }
}
