# Answer of BFI Test 

## Question
- [Question A](https://gitlab.com/snippets/1738867)
- [Question B](https://gitlab.com/snippets/1738877)
- [Question C](https://gitlab.com/snippets/1738884)

## Requirement
- dotnet core 2.x

## How to Run
- clone
```
$ git clone git@gitlab.com:ekasinggih/bfi-test.git
```

- run test
```
$ cd bfi-test/bfi-test.test/
$ dotnet test
```

Output should be 
```
Welcome to .NET Core!
---------------------
Learn more about .NET Core: https://aka.ms/dotnet-docs
Use 'dotnet --help' to see available commands or visit: https://aka.ms/dotnet-cli-docs

Telemetry
---------
The .NET Core tools collect usage data in order to help us improve your experience. The data is anonymous and doesn't include command-line arguments. The data is collected by Microsoft and shared with the community. You can opt-out of telemetry by setting the DOTNET_CLI_TELEMETRY_OPTOUT environment variable to '1' or 'true' using your favorite shell.

Read more about .NET Core CLI Tools telemetry: https://aka.ms/dotnet-cli-telemetry

ASP.NET Core
------------
Successfully installed the ASP.NET Core HTTPS Development Certificate.
To trust the certificate run 'dotnet dev-certs https --trust' (Windows and macOS only). For establishing trust on other platforms refer to the platform specific documentation.
For more information on configuring HTTPS see https://go.microsoft.com/fwlink/?linkid=848054.
Build started, please wait...
Build completed.

Test run for $/home/ekasinggih/bfi-test/bfi-test.test/bin/Debug/netcoreapp2.1/bfi-test.test.dll(.NETCoreApp,Version=v2.1)
Microsoft (R) Test Execution Command Line Tool Version 15.9.0
Copyright (c) Microsoft Corporation.  All rights reserved.

Starting test execution, please wait...

Total tests: 9. Passed: 9. Failed: 0. Skipped: 0.
Test Run Successful.
Test execution time: 31.4834 Seconds
```